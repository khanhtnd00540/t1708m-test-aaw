package com.T1708M.controller;


import com.T1708M.entity.Product;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import java.util.ArrayList;
import java.util.Arrays;

@SessionScoped
@ManagedBean(name = "productController")
public class ProductController {
    private   String name = "";
    private   String address = "";
    private   String phoneNumber= "";

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    private  ArrayList<Product> products
            = new ArrayList<Product>(Arrays.asList(
            new Product("vp1", "em yeu thay ", 123,"VIP"),
            new Product("vp2", " Thay hung dep zai", 345,"VIP"),
            new Product("vp3", "ahihi", 567,"VIP"),
            new Product("vp4", "Co nhung giau ten", 8910,"VIP"),
            new Product("vp5", "Em gai thay hung", 111213,"VIP")

    ));

    public  ArrayList<Product> getProducts() {
        return products;
    }

    public String index() {

        return "index?faces-redirect=true";
    }




    }


